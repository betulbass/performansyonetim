package com.sha.springbootdeneme.service;

import com.sha.springbootdeneme.model.Mustem;

import java.util.List;

public interface IMustemService {
    Mustem saveMustem(Mustem mustem);

    void deleteMustem(Long id);

    List<Mustem> findAllMustem();
}
