package com.sha.springbootdeneme.service;

import com.sha.springbootdeneme.model.Mustem;
import com.sha.springbootdeneme.repository.IMustemRepository;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service  // Belirtilen sınıfın bir servis sınıfı olduğunu belirtir.Bir beaninin business katmanında çalışacak bir bean olduğunu belirtiyoruz. Java EE’deki Business Service Facade işlevine karşılık gelmektedir.
public class MustemService implements IMustemService {
    private final IMustemRepository mustemRepository;

    public MustemService(IMustemRepository mustemRepository) {
        this.mustemRepository = mustemRepository;
    }
@Override
    public Mustem saveMustem(Mustem mustem){
        mustem.setTarih(LocalDate.now());
        return mustemRepository.save(mustem);

    }
    @Override
    public void deleteMustem(Long id){
        mustemRepository.deleteById(id);
    }
    @Override
    public List<Mustem>findAllMustem(){
        return mustemRepository.findAll();
    }

}


