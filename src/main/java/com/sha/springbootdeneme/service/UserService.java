package com.sha.springbootdeneme.service;

import com.sha.springbootdeneme.model.Role;
import com.sha.springbootdeneme.model.User;
import com.sha.springbootdeneme.repository.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class UserService implements IUserService{
    @Autowired
    private IUserRepository userRepository;
    @Autowired // Constructor, Değişken yada setter metodlar için dependency injection işlemi gerçekleştirir
    private PasswordEncoder passwordEncoder;

    @Override//kısıtlama kullanım tanımlayan anotasyon
    public User saveUser(User user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(Role.USER);
        user.setCreateTime(LocalDateTime.now());

        return userRepository.save(user);

    }

    @Override
    public Optional<User> findByUsername(String userName){
        return userRepository.findByUsername(userName);
    }
    @Override
    @Transactional //springte güncelleme veya silme sorgusu yürütürken zorunludur.  
    public void makeAdmin(String username){
        userRepository.updateUserRole(username,Role.ADMIN);
    }

}
