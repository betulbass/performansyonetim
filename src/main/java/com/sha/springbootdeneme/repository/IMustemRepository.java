package com.sha.springbootdeneme.repository;

import com.sha.springbootdeneme.model.Mustem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IMustemRepository  extends JpaRepository<Mustem,Long> {

}
