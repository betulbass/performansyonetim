package com.sha.springbootdeneme.repository;

import com.sha.springbootdeneme.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import com.sha.springbootdeneme.model.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface IUserRepository extends JpaRepository<User,Long>
{
    Optional<User> findByUsername(String username);

    @Modifying
    //Herhangi bir sorguyu tanımlamak için @Query anotasyonu kullanılır. Ancak, sorgunuz verileri değiştirdiğinde, yönteme @Modifying anotasyonu eklemek gerekir. UPDATE, INSERT veya DELETE kullanılıyosa, @Modifying ile metoda anotasyon eklemek gerekiyor.
    @Query("update User set role = :role where username = :username")
    void updateUserRole(@Param("username") String username, @Param("role") Role role);

}
