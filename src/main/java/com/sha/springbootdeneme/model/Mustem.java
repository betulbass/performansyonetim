package com.sha.springbootdeneme.model;

import lombok.Data;
//import org.springframework.data.annotation.Id;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalTime;
@Data
@Entity
@Table(name="mus_tem")

public class Mustem
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "mt_name", nullable = false, length = 100)
    private String mt_name;

    @Column(name = "tarih", nullable = false)
    private LocalDate tarih;

    @Column(name = "bas_saat", nullable = false)
    private LocalTime bas_saat ;

    @Column(name = "bit_saat", nullable = false)
    private LocalTime bit_saat ;

    @Column(name = "mazeret", nullable = false, length = 100)
    private String mazeret;

    @Column(name = "mazeret_saati", nullable = false, length = 100)
    private int mazeret_saati;

}
